# coding: utf-8
from __future__ import (
    absolute_import, division, print_function, unicode_literals
)

import math
from collections import namedtuple
from operator import attrgetter

Destination = namedtuple('Destination', 'name latitude longitude passions passions_satisfied')
CoupledDestinations = namedtuple('CoupledDestinations', 'name passions_satisfied_count distance_between')
Point = namedtuple('Point', 'latitude longitude')


def dist(point1, point2):
    EARTH_RADIUS = 6371  # in km
    point1_lat_in_radians = math.radians(point1.latitude)
    point2_lat_in_radians = math.radians(point2.latitude)
    point1_long_in_radians = math.radians(point1.longitude)
    point2_long_in_radians = math.radians(point2.longitude)
    return math.acos(
        math.sin(point1_lat_in_radians) * math.sin(point2_lat_in_radians) +
        math.cos(point1_lat_in_radians) * math.cos(point2_lat_in_radians) *
        math.cos(point2_long_in_radians - point1_long_in_radians)
    ) * EARTH_RADIUS


guest_count = int(input())
passions = set()
for _ in range(guest_count):
    guest_data = input().split()
    passion_count = int(guest_data.pop(0))
    assert passion_count == len(guest_data)
    passions.update(guest_data)

destinations = []
destination_count = int(input())
for _ in range(destination_count):
    destination_data = input().split()
    # convert floats
    destination_data[1] = float(destination_data[1])
    destination_data[2] = float(destination_data[2])
    # read passions
    passion_count = int(destination_data.pop(3))
    destination_data[3:] = [set(destination_data[3:])]
    assert passion_count == len(destination_data[3])
    assert len(destination_data) == 4
    destinations.append(Destination(
        *(destination_data + [passions & destination_data[3]])
    ))

destinations.sort(key=lambda dest: len(dest.passions_satisfied), reverse=True)

max_coupled_passions_count = 0
# now we can couple pairs of destinations starting from the most promising
dest_couples = []
for i in range(len(destinations) - 1):
    dest1 = destinations[i]
    for couple_index in range(i + 1, len(destinations)):
        dest2 = destinations[couple_index]
        coupled_passions_count = len(dest1.passions_satisfied | dest2.passions_satisfied)
        if not max_coupled_passions_count:
            max_coupled_passions_count = coupled_passions_count
        if coupled_passions_count < max_coupled_passions_count:
            break
        name = ' '.join(sorted([dest1.name, dest2.name]))
        distance_between = dist(Point(dest1.latitude, dest1.longitude), Point(dest2.latitude, dest2.longitude))
        dest_couples.append(CoupledDestinations(name, coupled_passions_count, distance_between))

dest_couples.sort(key=attrgetter('distance_between'))
dest_couples.sort(key=attrgetter('passions_satisfied_count'), reverse=True)
print(dest_couples[0].name)
