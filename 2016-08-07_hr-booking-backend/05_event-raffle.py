# coding: utf-8
from __future__ import (
    absolute_import, division, print_function, unicode_literals
)

from collections import namedtuple


class Person(namedtuple('Person', 'event_id passions')):

    @property
    def happy(self):
        return bool(events[self.event_id] & self.passions)


def try_swap():
    swapped = False
    for i in range(len(persons) - 1):
        person1 = persons[i]
        for j in range(i + 1, len(persons)):
            person2 = persons[j]
            happy_count = int(person1.happy) + int(person2.happy)
            after_swap_person1 = person1._replace(event_id=person2.event_id)
            after_swap_person2 = person2._replace(event_id=person1.event_id)
            after_swap_happy_count = int(after_swap_person1.happy) + int(after_swap_person2.happy)
            if after_swap_happy_count > happy_count:
                persons[i] = person1 = after_swap_person1
                persons[j] = after_swap_person2
                swapped = True
    return swapped


n = int(input())

persons = []
for _ in range(n):
    person_data = input().split()
    # calculate event_id
    person_data[0] = int(person_data[0]) - 1
    passion_count = int(person_data.pop(1))
    person_data[1:] = [set(person_data[1:])]
    assert passion_count == len(person_data[1])
    persons.append(Person(*person_data))

events = []
for _ in range(n):
    passion_data = input().split()
    passion_count = int(passion_data.pop(0))
    assert passion_count == len(passion_data)
    events.append(set(passion_data))

swapped = True
while swapped:
    swapped = try_swap()

print(sum(1 for person in persons if person.happy))
