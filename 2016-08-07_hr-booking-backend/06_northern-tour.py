# coding: utf-8
from __future__ import (
    absolute_import, division, print_function, unicode_literals
)

from collections import defaultdict


INITIAL_CITY = 'Bevagna'
TIME_RESOURCE = 6 * 24


def hours_24(hours_passed):
    return (hours_passed + 8) % 24


def compute_departure_time(arrival_time, hours_to_visit):
    # assume we have day-light time here
    assert hours_24(arrival_time) >= 8
    remaining_time = hours_to_visit
    current_time = arrival_time
    while remaining_time > 0:
        till_dusk = 24 - hours_24(current_time)
        if remaining_time < till_dusk:
            return current_time + remaining_time
        remaining_time -= till_dusk
        current_time += till_dusk + 8
    return current_time


def next_cities(visited, hours_passed):
    """
    :param city:
    :param hours_passed: starting from initial 8 am
    :return: list of next cities
    """
    city = visited[-1]
    max_route = []
    for dest_city, hours_to_fly in routes[city].items():
        # we cannot visit a city twice
        if dest_city in visited:
            continue
        # suppose we are going to fly there
        arrival_time = hours_passed + hours_to_fly
        arrival_time_24 = hours_24(arrival_time)
        # we cannot sleep during flight and must arrive till the dusk
        if 0 < arrival_time_24 < 8:
            continue
        departure_time = compute_departure_time(arrival_time, cities[dest_city])
        # we cannot finish visiting city later than sunday 8 am
        if departure_time > TIME_RESOURCE:
            continue
        # when all restrictions are met, we can finally fly there!
        route = [dest_city] + next_cities(visited + [dest_city], departure_time)
        if len(route) > len(max_route):
            max_route = route
    return max_route


cities_count = int(input())
cities = {}
for _ in range(cities_count):
    city_data = input().split(',')
    cities[city_data[0]] = int(city_data[1])

routes_count = int(input())
routes = defaultdict(dict)
for _ in range(routes_count):
    route_data = input().split(',')
    city1 = route_data[0]
    city2 = route_data[1]
    routes[city1][city2] = int(route_data[2])
    routes[city2][city1] = int(route_data[2])

cities_route = next_cities([INITIAL_CITY], 0)
if not cities_route:
    print('NONE')
else:
    for city in cities_route:
        print(city)
