# coding: utf-8
from __future__ import (
    absolute_import, division, print_function, unicode_literals
)
# solution to: https://www.hackerrank.com/contests/booking-passions-hacked-backend/challenges/destinations-together-3
from math import factorial

n, m, c = (int(v) for v in input().split())
unique_cities_count = n + m - c
print(factorial(unique_cities_count - 1))