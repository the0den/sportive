# coding: utf-8
from __future__ import (
    absolute_import, division, print_function, unicode_literals
)

from collections import namedtuple
from operator import attrgetter

from datetime import date

Reviewer = namedtuple('Reviewer', 'mentioned_reviews_count score rev_id')


def compute_score(timestamp, review_length):
    score = 10
    # if score between dates then 20
    if date(2016, 6, 15) <= date.fromtimestamp(timestamp) <= date(2016, 7, 15):
        score = 20
    score += 20 if review_length >= 100 else 10
    return score


passion_cnt, review_cnt = (int(v) for v in input().split())
passions = [input().lower() for _ in range(passion_cnt)]
passions_dict = {p: {} for p in passions}

for _ in range(review_cnt):
    rev_id, timestamp = (int(v) for v in input().split())
    review_text = input().lower()
    for passion in passions:
        if not passion in review_text:
            continue
        score = compute_score(timestamp, len(review_text))
        old_reviewer = passions_dict[passion].get(rev_id, Reviewer(0, 0, rev_id))
        passions_dict[passion][rev_id] = Reviewer(
            old_reviewer.mentioned_reviews_count + 1,
            old_reviewer.score + score,
            rev_id
        )

# print results
for passion in passions:
    reviewers_rating = sorted(
        [reviewer for reviewer in passions_dict[passion].values()],
        key=attrgetter('rev_id')
    )
    if not reviewers_rating:
        print('-1')
        continue
    reviewers_rating.sort(key=attrgetter('score'), reverse=True)
    print(reviewers_rating[0].rev_id)
