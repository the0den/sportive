# coding: utf-8
from __future__ import (
    absolute_import, division, print_function, unicode_literals
)
# My solution for https://www.hackerrank.com/challenges/pangrams

from collections import Counter
from string import ascii_lowercase

ANSWERS = {True: 'pangram', False: 'not pangram'}
text = input().lower()
c = Counter(text)
print(ANSWERS[all(c[char] for char in ascii_lowercase)])
