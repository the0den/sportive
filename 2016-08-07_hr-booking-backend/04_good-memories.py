# coding: utf-8
from __future__ import (
    absolute_import, division, print_function, unicode_literals
)

# solution to https://www.hackerrank.com/contests/booking-passions-hacked-backend/challenges/good-memories
from collections import namedtuple


Site = namedtuple('Site', 'sites_before sites_after')


def get_site(i, sites):
    sites_before = set(sites[:i])
    sites_after = set(sites[i + 1:])
    return Site(sites_before, sites_after)



def process_excursion():
    num_friends = int(input())
    violated = False
    sites_dict = {}
    for _ in range(num_friends):
        sites = input().lower().split(',')
        if violated:
            continue
        for i, site_name in enumerate(sites):
            site = get_site(i, sites)
            old_site = sites_dict.get(site_name, Site(set(), set()))
            if site.sites_before & old_site.sites_after or site.sites_after & old_site.sites_before:
                print('ORDER VIOLATION')
                violated = True
                break
            sites_dict[site_name] = Site(
                old_site.sites_before | site.sites_before,
                old_site.sites_after | site.sites_after
            )
    if not violated:
        print('ORDER EXISTS')


excursions_count = int(input())
for _ in range(excursions_count):
    process_excursion()